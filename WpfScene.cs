﻿/**     Obligatorisk oppgave GRAFDA60 
 *      2012: Terje Rene Nilsen 
 *      terje.nilsen@student.hive.no
 *      
 *      Mine kommentarer er på engelsk.
 *      
 *      Koden til WpfRobotBody.randomNumber() er 
 *      opprinnelig fra Marc Gravell.
 *      Referanse gitt ved den aktuelle koden.
 *      
 *      Aksekorset er koden til Charles Petzold.
 *      Aksekorset inkl. følgende filer:
 *      Axes.cs, StrokeCharacters.cs, TextGenerator.cs
 *      Petzold.Media3D.dll
 *      
 *      Terje Rene Nilsen (TN)
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WpfRobot
{
    class WpfScene
    {
        public static double sceneSize = 20;
    }
}
