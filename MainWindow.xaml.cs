﻿/**     Obligatorisk oppgave GRAFDA60 
 *      2012: Terje Rene Nilsen 
 *      terje.nilsen@student.hive.no
 *      
 *      Mine kommentarer er på engelsk.
 *      
 *      Koden til WpfRobotBody.randomNumber() er 
 *      opprinnelig fra Marc Gravell.
 *      Referanse gitt ved den aktuelle koden.
 *      
 *      Aksekorset er koden til Charles Petzold.
 *      Aksekorset inkl. følgende filer:
 *      Axes.cs, StrokeCharacters.cs, TextGenerator.cs
 *      Petzold.Media3D.dll
 *      
 *      Terje Rene Nilsen (TN)
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Media.Media3D;
using System.Windows.Media.Animation;



namespace WpfRobot
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Viewport3D viewport;

        int cameraAngle = 1;
        String cameraType = "perspective";

        WpfRobotBody robot;
        WpfRobotBody robotTwo;

        Point3D lookat = new Point3D(0, 0, 0);

        Storyboard storyBoard = new Storyboard();

        Axes axMy; // Axes, TN

        // default values, TN
        double kneeDegree = -9;
        double hipDegree = 10;
        double shoulderDegree = -16;
        double elbowDegree = 14;

        //race values, TN

        bool race = false; // are we having a race now?
        bool stadiumRace = true; // selected by radiobuttons 
        int rndOpponentType;
        int opponentType = 0; //    in race-tab

        double animationSpeed = 0.4;

        String opponentTypeString; // opponent type

        public DirectionalLight positionLight(Point3D position)
        {
            DirectionalLight directionalLight = new DirectionalLight();
            directionalLight.Color = Colors.Gray;
            directionalLight.Direction = new Point3D(0, 0, 0) - position;
            return directionalLight;
        }

        public DirectionalLight leftLight()
        {
            return positionLight(new Point3D(-WpfScene.sceneSize, WpfScene.sceneSize / 2, 0.0));
        }

        public void handleCamera(object sender, RoutedEventArgs e)
        {
            if (cameraType == "perspective")
            {
                PerspectiveCamera theNewCamera = new PerspectiveCamera(new Point3D(0, 0, -30), new Vector3D(0, 0, 1), new Vector3D(0, 1, 0), 60);

                if (cameraAngle == 0) // overhead
                {
                    lblCam.Content = "Camera angle: " + "Overhead";
                    theNewCamera.Position = new Point3D(0, WpfScene.sceneSize * 2, WpfScene.sceneSize / 50);
                }
                if (cameraAngle == 1) // sideways
                {
                    lblCam.Content = "Camera angle: " + "Sideways";
                    theNewCamera.Position = new Point3D(-WpfScene.sceneSize, WpfScene.sceneSize / 2, WpfScene.sceneSize);
                }
                if (cameraAngle == 2) // xy-plane
                {
                    lblCam.Content = "Camera angle: " + "XY-plane";
                    theNewCamera.Position = new Point3D(WpfScene.sceneSize * 2, 0, WpfScene.sceneSize / 50);
                }

                theNewCamera.LookDirection = new Vector3D(lookat.X - theNewCamera.Position.X,
                                                               lookat.Y - theNewCamera.Position.Y,
                                                               lookat.Z - theNewCamera.Position.Z);

                lblCamType.Content = "Camera type: Perspective";
                viewport.Camera = theNewCamera;
            }
            else if (cameraType == "orthographic") {
                OrthographicCamera theNewCamera = new OrthographicCamera(new Point3D(0, 0, -30), new Vector3D(0, 0, 1), new Vector3D(0, 1, 0),60);

                if (cameraAngle == 0) // overhead
                {
                    lblCam.Content = "Camera angle: " + "Overhead";
                    theNewCamera.Position = new Point3D(0, WpfScene.sceneSize * 2, WpfScene.sceneSize / 50);
                }
                if (cameraAngle == 1) // sideways
                {
                    lblCam.Content = "Camera angle: " + "Sideways";
                    theNewCamera.Position = new Point3D(-WpfScene.sceneSize, WpfScene.sceneSize / 2, WpfScene.sceneSize);
                }
                if (cameraAngle == 2) // xy-plane
                {
                    lblCam.Content = "Camera angle: " + "XY-plane";
                    theNewCamera.Position = new Point3D(WpfScene.sceneSize * 1, 0, 0);
                }

                theNewCamera.LookDirection = new Vector3D(lookat.X - theNewCamera.Position.X,
                                                               lookat.Y - theNewCamera.Position.Y,
                                                               lookat.Z - theNewCamera.Position.Z+5);
                lblCamType.Content = "Camera type: Orthographic";
                viewport.Camera = theNewCamera;
            }
            
        }
        
        public MainWindow()
        {
            InitializeComponent();
        }


        private void Viewport3D_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is Viewport3D)
            {
                viewport = (Viewport3D)sender;

                robot = new WpfRobotBody();

                double floorThickness = WpfScene.sceneSize / 100;
                GeometryModel3D floorModel = WpfCube.CreateCubeModel(
                    new Point3D(-WpfScene.sceneSize / 2,
                                -floorThickness,
                                -WpfScene.sceneSize / 2),
                    WpfScene.sceneSize, floorThickness, WpfScene.sceneSize, Colors.Tan);

                Model3DGroup groupScene = new Model3DGroup();

                groupScene.Children.Add(floorModel);

                groupScene.Children.Add(robot.getModelGroup());

                groupScene.Children.Add(leftLight());
                groupScene.Children.Add(new AmbientLight(Colors.Gray));

                //viewport.Camera = camera();
                handleCamera(sender, e);

                ModelVisual3D visual = new ModelVisual3D();
                visual.Content = groupScene;
                viewport.Children.Add(visual);

                updateArms();
                updateLegs();

                storyboardRobot(robot, 3.4);
            }
        }


        /** emptyViewportChildren(Viewport3D thisView)
         * Removes everything children from the viewport.
         * TN
         * */
        private void emptyViewportChildren(Viewport3D thisView)
        {
            int nrOfChildren = thisView.Children.Count();

            for (int i = 0; i < nrOfChildren; i++)
            {
                thisView.Children.RemoveAt(0);
            }
        }

        private void resetBoard(Viewport3D thisView)
        {
           // robot = new WpfRobotBody();

            double floorThickness = WpfScene.sceneSize / 100;
            GeometryModel3D floorModel = WpfCube.CreateCubeModel(
                new Point3D(-WpfScene.sceneSize / 2,
                            -floorThickness,
                            -WpfScene.sceneSize / 2),
                WpfScene.sceneSize, floorThickness, WpfScene.sceneSize, Colors.Tan);

            Model3DGroup groupScene = new Model3DGroup();

            groupScene.Children.Add(floorModel);
            groupScene.Children.Add(leftLight());
            groupScene.Children.Add(new AmbientLight(Colors.Gray));

            //thisView.Camera = camera();

            ModelVisual3D visual = new ModelVisual3D();

            visual.Content = groupScene;

            thisView.Children.Add(visual);

        }

        private int durationM(double seconds)
        {
            int milliseconds = (int)(seconds * 1000);
            return milliseconds;
        }

        public TimeSpan durationTS(double seconds)
        {
            TimeSpan ts = new TimeSpan(0, 0, 0, 0, durationM(seconds));
            return ts;
        }

        private void storyboardRobot(WpfRobotBody Robot, double speed)
        {
            double turnDuration = 0.3; // how much time to use in a corner. TN
            double totalDuration = 0.0; // how much time spent in the center before starting story. TN
            double walkDuration = speed;// how fast to walk. TN default: 3.4;

            NameScope.SetNameScope(this, new NameScope());

            storyBoard = new Storyboard();
            // new storyboard each time we make a robot cannot be a good thing..
            // todo: must rethink this! TN

            axMy = new Axes();

            Vector3D vector = new Vector3D(0, 1, 0);

            AxisAngleRotation3D rotation = new AxisAngleRotation3D(vector, 0.0);
            Robot.getRotateTransform().Rotation = rotation;

            DoubleAnimation doubleAnimationTurn1 = new DoubleAnimation(0.0, 90.0, durationTS(turnDuration));
            DoubleAnimation doubleAnimationTurn2 = new DoubleAnimation(90.0, 180.0, durationTS(turnDuration));
            DoubleAnimation doubleAnimationTurn3 = new DoubleAnimation(180.0, 270.0, durationTS(turnDuration));
            DoubleAnimation doubleAnimationTurn4 = new DoubleAnimation(270.0, 360.0, durationTS(turnDuration));

            RegisterName("TurnRotation", rotation);

            RegisterName("MoveTransform", Robot.getTranslateTransform());


            storyBoard.Children.Add(doubleAnimationTurn1);
            if ((race.Equals(false)) || (stadiumRace.Equals(true)))
            {
                storyBoard.Children.Add(doubleAnimationTurn2);
                storyBoard.Children.Add(doubleAnimationTurn3);
                storyBoard.Children.Add(doubleAnimationTurn4);
            }
            
            Storyboard.SetTargetName(doubleAnimationTurn1, "TurnRotation");
            Storyboard.SetTargetProperty(doubleAnimationTurn1, new PropertyPath(AxisAngleRotation3D.AngleProperty));
            Storyboard.SetTargetName(doubleAnimationTurn2, "TurnRotation");
            Storyboard.SetTargetProperty(doubleAnimationTurn2, new PropertyPath(AxisAngleRotation3D.AngleProperty));
            Storyboard.SetTargetName(doubleAnimationTurn3, "TurnRotation");
            Storyboard.SetTargetProperty(doubleAnimationTurn3, new PropertyPath(AxisAngleRotation3D.AngleProperty));
            Storyboard.SetTargetName(doubleAnimationTurn4, "TurnRotation");
            Storyboard.SetTargetProperty(doubleAnimationTurn4, new PropertyPath(AxisAngleRotation3D.AngleProperty));


            double offset = WpfScene.sceneSize * 0.45;



            DoubleAnimation doubleAnimationX1 = new DoubleAnimation(-offset, -offset, durationTS(walkDuration));
            DoubleAnimation doubleAnimationZ1 = new DoubleAnimation(-offset, offset, durationTS(walkDuration));
            Storyboard.SetTargetName(doubleAnimationX1, "MoveTransform");
            Storyboard.SetTargetProperty(doubleAnimationX1, new PropertyPath(TranslateTransform3D.OffsetXProperty));
            Storyboard.SetTargetName(doubleAnimationZ1, "MoveTransform");
            Storyboard.SetTargetProperty(doubleAnimationZ1, new PropertyPath(TranslateTransform3D.OffsetZProperty));
            storyBoard.Children.Add(doubleAnimationX1);
            storyBoard.Children.Add(doubleAnimationZ1);

                DoubleAnimation doubleAnimationX2 = new DoubleAnimation(-offset, offset, durationTS(walkDuration));
                DoubleAnimation doubleAnimationZ2 = new DoubleAnimation(offset, offset, durationTS(walkDuration));

            
                Storyboard.SetTargetName(doubleAnimationX2, "MoveTransform");
                Storyboard.SetTargetProperty(doubleAnimationX2, new PropertyPath(TranslateTransform3D.OffsetXProperty));
                Storyboard.SetTargetName(doubleAnimationZ2, "MoveTransform");
                Storyboard.SetTargetProperty(doubleAnimationZ2, new PropertyPath(TranslateTransform3D.OffsetZProperty));
                if ((race.Equals(false)) || (stadiumRace.Equals(true)))
                {
                    storyBoard.Children.Add(doubleAnimationX2);
                    storyBoard.Children.Add(doubleAnimationZ2);
                }

                DoubleAnimation doubleAnimationX3 = new DoubleAnimation(offset, offset, durationTS(walkDuration));
                DoubleAnimation doubleAnimationZ3 = new DoubleAnimation(offset, -offset, durationTS(walkDuration));
                
                Storyboard.SetTargetName(doubleAnimationX3, "MoveTransform");
                Storyboard.SetTargetProperty(doubleAnimationX3, new PropertyPath(TranslateTransform3D.OffsetXProperty));
                Storyboard.SetTargetName(doubleAnimationZ3, "MoveTransform");
                Storyboard.SetTargetProperty(doubleAnimationZ3, new PropertyPath(TranslateTransform3D.OffsetZProperty));
                if ((race.Equals(false)) || (stadiumRace.Equals(true)))
                {
                    storyBoard.Children.Add(doubleAnimationX3);
                    storyBoard.Children.Add(doubleAnimationZ3);
                }

                DoubleAnimation doubleAnimationX4 = new DoubleAnimation(offset, -offset, durationTS(walkDuration));
                DoubleAnimation doubleAnimationZ4 = new DoubleAnimation(-offset, -offset, durationTS(walkDuration));
                
                Storyboard.SetTargetName(doubleAnimationX4, "MoveTransform");
                Storyboard.SetTargetProperty(doubleAnimationX4, new PropertyPath(TranslateTransform3D.OffsetXProperty));
                Storyboard.SetTargetName(doubleAnimationZ4, "MoveTransform");
                Storyboard.SetTargetProperty(doubleAnimationZ4, new PropertyPath(TranslateTransform3D.OffsetZProperty));
                if ((race.Equals(false)) || (stadiumRace.Equals(true)))
                {
                    storyBoard.Children.Add(doubleAnimationX4);
                    storyBoard.Children.Add(doubleAnimationZ4);
                }
           
            doubleAnimationX1.BeginTime = durationTS(totalDuration);
            doubleAnimationZ1.BeginTime = durationTS(totalDuration);
            totalDuration += walkDuration;
             
                doubleAnimationTurn1.BeginTime = durationTS(totalDuration);
                totalDuration += turnDuration;
           
                doubleAnimationX2.BeginTime = durationTS(totalDuration);
                doubleAnimationZ2.BeginTime = durationTS(totalDuration);
                totalDuration += walkDuration;

                doubleAnimationTurn2.BeginTime = durationTS(totalDuration);
                totalDuration += turnDuration;

                doubleAnimationX3.BeginTime = durationTS(totalDuration);
                doubleAnimationZ3.BeginTime = durationTS(totalDuration);
                totalDuration += walkDuration;

                doubleAnimationTurn3.BeginTime = durationTS(totalDuration);
                totalDuration += turnDuration;

                doubleAnimationX4.BeginTime = durationTS(totalDuration);
                doubleAnimationZ4.BeginTime = durationTS(totalDuration);
                totalDuration += walkDuration;

                doubleAnimationTurn4.BeginTime = durationTS(totalDuration);
                totalDuration += turnDuration;

                // note to later http://pastebin.com/xupTFba1 err.. 
                // : geocodeService.GeocodeCompleted += new EventHandler((object sender, GeocodeService.GeocodeCompletedEventArgs e) => geocodeService_GeocodeCompleted(sender, e, id));


            // Make sure the race ends after 1 lap, TN
            if (race.Equals(true))
            {
                storyBoard.RepeatBehavior = new RepeatBehavior(1.0);
                storyBoard.AutoReverse = false;
                storyBoard.Completed += new EventHandler(End_Of_Story);
            }
            else
            {
                storyBoard.RepeatBehavior = RepeatBehavior.Forever;
                storyBoard.SpeedRatio = 0.5;
            }


            // true, true == (isControllable == true), TN
            storyBoard.Begin(this, true);
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (sender is MainWindow)
            {
                Rect r = SystemParameters.WorkArea;

                MainWindow window = (MainWindow)sender;

                window.Width = r.Width;
                window.Height = r.Height;
                window.Left = 0;
                window.Top = 0;
            }
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void btnStoryboard_Click(object sender, RoutedEventArgs e)
        {
            
            if ((String)btnStoryboard.Content == "Pause")
            {
                btnStoryboard.Content = "Continue";
                

                storyBoard.Pause(this);
                robot.workLegs(hipDegree, kneeDegree, 0.0);
                robot.workArms(elbowDegree, shoulderDegree, 0.0);
                
            }
            else if ((String)btnStoryboard.Content == "Continue")
            {
                btnStoryboard.Content = "Pause";
                storyBoard.Resume(this);

                updateLegs();
                updateArms();
            }
            else
            { // reset

                emptyViewportChildren(viewport);
                resetBoard(viewport);
                btnStoryboard.Content = "Pause";
                robot = new WpfRobotBody();
                Model3DGroup groupScene = new Model3DGroup();

                groupScene.Children.Add(robot.getModelGroup());

                ModelVisual3D visual = new ModelVisual3D();

                visual.Content = groupScene;

                viewport.Children.Add(visual);
                storyboardRobot(robot, 3.5);

                updateLegs();
                updateArms();
            }
        }

        private void btnAxis_Click(object sender, RoutedEventArgs e)
        {
            if ((String)btnAxis.Content == "Show axes")
            {
                btnAxis.Content = "Remove axes";
                viewport.Children.Add(axMy);
            }
            else
            {
                btnAxis.Content = "Show axes";
                viewport.Children.Remove(axMy);
            }
        }

        private void btnChgCam_Click(object sender, RoutedEventArgs e)
        {
            String strCamera = "Camera: ";
            if ((String)lblCam.Content == strCamera + "Sideways")
            {
                lblCam.Content = strCamera + "Overhead";
                // Set camera = overhead
                cameraAngle = 0;
                //viewport.Camera = camera();
            }
            else if ((String)lblCam.Content == strCamera + "Overhead")
            {
                lblCam.Content = strCamera + "XZ-Plane";
                // Set camera = XZ-Plane
                cameraAngle = 2;
                //viewport.Camera = camera();
            }
            else
            {
                lblCam.Content = strCamera + "Sideways";
                // Set camera = Sideways
                cameraAngle = 1;
               // viewport.Camera = camera();
            }
                //viewport.Camera = camera();
//ToggleCamera(sender, e);
        }

        private void sldHip_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            hipDegree = sldHip.Value;
            updateLegs();
        }

        private void sldKnee_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            kneeDegree = sldKnee.Value;
            updateLegs();
        }

        private void sldElbow_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            elbowDegree = sldElbow.Value;
            updateArms();
        }

        private void sldShoulder_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            shoulderDegree = sldShoulder.Value;
            updateArms();
        }

        /** updateLegs():
         * Updates robot and (if in race) robotTwo
         * Makes GUI updates
         * 
         * Should be made OO.
         * TN
         * */
        private void updateLegs()
        {
            try
            {
                robot.workLegs(hipDegree, (0 - kneeDegree), animationSpeed);
                if (race.Equals(true))
                {
                    robotTwo.workLegs(hipDegree, (0 - kneeDegree), animationSpeed);
                }
                lblKnee.Content = "Knee degree: " + (int)sldKnee.Value;
                lblHip.Content = "Hip degree: " + (int)sldHip.Value;
            }
            catch (NullReferenceException ex)
            {
                // lets not confuse the user, everything is ok..
            }
        }

        /** updateArms():
         * Updates robot and (if in race) robotTwo
         * Makes GUI updates
         * 
         * Should be made OO.
         * TN
         * */
        private void updateArms()
        {
            try
            {
                // robot.workArms(

                robot.workArms((0 - elbowDegree), (shoulderDegree), animationSpeed);
                if (race.Equals(true))
                {
                    robotTwo.workArms((0 - elbowDegree), (shoulderDegree), animationSpeed);
                }
                lblElbow.Content = "Elbow degree: " + (int)sldElbow.Value;
                lblShoulder.Content = "Shoulder degree: " + (int)sldShoulder.Value;

            }
            catch (NullReferenceException ex)
            {
                // lets not confuse the user, everything is ok..
            }
        }

        /*
        //Toggle between camera projections. TN
        public void ToggleCamera(object sender, EventArgs e)
        {
            String strCamera = "Camera: ";
            if ((bool)CameraCheck.IsChecked == true)
            {
                /*
                OrthographicCamera myOCamera = new OrthographicCamera(new Point3D(0, 0, -30), new Vector3D(0, 0, 1), new Vector3D(0, 1, 0), 25);
                
                btnChgCam.IsEnabled = false*/
          /*      OrthographicCamera myOCamera = OrthoCamera();
                lblCam.Content = strCamera + "Sideways";
                lblCamType.Content = "Camera type: Orthographic";
                viewport.Camera = myOCamera;

            }
            if ((bool)CameraCheck.IsChecked != true)
            {
                btnChgCam.IsEnabled = true;
              //  cameraAngle = 1;
                PerspectiveCamera myPCamera = camera();
                viewport.Camera = myPCamera;
                lblCamType.Content = "Camera type: Perspective";
            }
        }*/

        private void sldSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                animationSpeed = 1.1 - (Math.Round(sldSpeed.Value, 1));
                updateLegs();
                updateArms();
                // +1.1 to make it look more understandable for the user..
                lblSpeed.Content = "Animation speed: " + (1.1 - animationSpeed);
            }
            catch (NullReferenceException ex)
            {
                // lets not confuse the user, everything is ok
            }
        }

        /** btnGoToRace_Click():
         * 3 states: "Make Ready", "Race!", "Stop race!"
         *      Make Ready:
         *          Clear and reset storyboard, reset winner fields, do GUI changes
         *      Race:
         *          Make robots with selected opponent type, add them to the board, 
         *          start the race :)
         *          Do GUI changes
         *      Stop race:
         *          Call storyBoard.Pause(this); and raceIsOver();
         *  TN
         * */
        private void btnGoToRace_Click(object sender, RoutedEventArgs e)

        {
            // btnGoToRace.Content = viewport.Children.Count(); //debug
            if (btnGoToRace.Content.Equals("Make ready!"))
            {
                // reset winners
                lblWinnerIs.Content = "";
                lblWinnerRobot.Content = "";
                lblWinnerColor.Visibility = System.Windows.Visibility.Hidden;

                // reset storyboard
                emptyViewportChildren(viewport);
                resetBoard(viewport);

                // GUI changes
                btnAxis.Content = "Show axes";
                btnGoToRace.Content = "Race!";

                rndOpponentType =  WpfRobotBody.randomNumber(1, 3);
            }
            else if (btnGoToRace.Content.Equals("Race!"))
            {
                race = true;
                if (radioStraightRace.IsChecked.Equals(true))
                {
                    stadiumRace = false;
                }
                else
                {
                    stadiumRace = true;
                }
                // Race on!

                double speedRobotOne = 3.5;
                double speedRobotTwo = 3.5;

                // a GUI changes
                radioEndurance.IsEnabled = false;
                radioOxygen.IsEnabled = false;
                radioSlowStart.IsEnabled = false;
                radioStraightRace.IsEnabled = false;
                radioStadiumRace.IsEnabled = false;
                radioRandom.IsEnabled = false;

                btnGoToRace.Content = "Stop race!";

                btnStoryboard.Content = "Reset";
                btnStoryboard.IsEnabled = false;

                // create the robot objects
                robotTwo = new WpfRobotBody();
                robot = new WpfRobotBody();
                Model3DGroup groupScene = new Model3DGroup();
                groupScene.Children.Add(robotTwo.getModelGroup());
                groupScene.Children.Add(robot.getModelGroup());
                ModelVisual3D visual = new ModelVisual3D();
                visual.Content = groupScene;
                viewport.Children.Add(visual);

                    int rndSpeed = WpfRobotBody.randomNumber(1, 4);
                    if (radioRandom.IsChecked.Equals(false)) {
                        // we're not going with random opponent type
                        rndOpponentType = 0;
                    }
                    if (radioSlowStart.IsChecked.Equals(true) || rndOpponentType.Equals(1))
                    {
                        opponentType = 0;
                            // 1.0 is added to give robotOne a slow start
                        speedRobotOne = (double)rndSpeed + 1.0;
                        speedRobotTwo = (double)rndSpeed;
                        opponentTypeString = "Slow start";
                    }
                    else if (radioOxygen.IsChecked.Equals(true) || rndOpponentType.Equals(2))
                    {
                        opponentType = 1;
                            // 0.15 is added to animate that robotOne gets tired after a while
                            // and start falling behind
                        speedRobotOne = (double)rndSpeed + 0.15;
                        speedRobotTwo = (double)rndSpeed;
                        opponentTypeString = "Oxygen uptake (gets tired)";
                    }
                    else if (radioEndurance.IsChecked.Equals(true) || rndOpponentType.Equals(3))
                    {
                        opponentType = 2;
                            // 0.02 is added so we can see both robots
                            // The idea is that both robots run equally fast. 
                        speedRobotOne = (double)rndSpeed + 0.02;
                        speedRobotTwo = (double)rndSpeed;
                        opponentTypeString = "Endurance (equally fast)";
                    }

                    lblRaceIsOn.Content = "Race is on!   Opponent type: " + opponentTypeString;

                    // make robot with desired speed
                    storyboardRobot(robot, speedRobotOne);
                    storyboardRobot(robotTwo, speedRobotTwo);

                    // give robots the user settings fron the play-tab
                    updateLegs();
                    updateArms();

            }
            else
            {
                // stop storyboard
                storyBoard.Pause(this);
                raceIsOver();
            }

        }

        /** raceIsOver()
         * a list of routines to do when a robot wins, or the game is stopped
         * TN
         * */
        private void raceIsOver()
        {
            robot.workLegs(hipDegree, kneeDegree, 0.0);
            robot.workArms(elbowDegree, shoulderDegree, 0.0);

            robotTwo.workLegs(hipDegree, kneeDegree, 0.0);
            robotTwo.workArms(elbowDegree, shoulderDegree, 0.0);

            tabControl.IsEnabled = true;
            radioEndurance.IsEnabled = true;
            radioOxygen.IsEnabled = true;
            radioSlowStart.IsEnabled = true;
            radioStraightRace.IsEnabled = true;
            radioStadiumRace.IsEnabled = true;
            radioRandom.IsEnabled = true;
            btnStoryboard.IsEnabled = true;
            btnGoToRace.Content = "Make ready!";
            lblRaceIsOn.Content = "Race is on: no";
            opponentTypeString = String.Empty;
            lblRaceIsOn.Visibility = System.Windows.Visibility.Visible;
            race = false;
        }


        /** End_Of_Story:
         * Is called when the storyboard is completed.
         * Should only be active when there is a race.
         * In storyboardRobot(): storyBoard.Completed += new EventHandler(End_Of_Story);
         * 
         * The winners: This is not an AI-course; robotTwo always wins.
         * 
         * The winner will be displayed with its shirt color, via [robot].getTorsoColor();
         * 
         * TN
         * */
        private void End_Of_Story(object sender, EventArgs e)
        {
            if (race.Equals(true))
            {
                // who won? how to tell?
                lblWinnerIs.Content = "The winner is:";
                if (opponentType.Equals(0) || opponentType.Equals(1))
                {
                    //robotTwo won! woohoo.. ;) TN

                    Brush tmp = new SolidColorBrush(robotTwo.getTorsoColor());
                    lblWinnerRobot.Content = "Robot ";
                    lblWinnerColor.Background = tmp;
                }
                else
                {
                    // both robots won, its a draw..
                    // use LinearGradentBrush to show both colors in one box :) TN
                    LinearGradientBrush twoColors = new LinearGradientBrush();
                    twoColors.StartPoint = new Point(0, 0);
                    twoColors.EndPoint = new Point(1, 1);

                    GradientStop robotOneColor = new GradientStop();
                    GradientStop robotTwoColor = new GradientStop();
                    robotOneColor.Color = robot.getTorsoColor();
                    robotTwoColor.Color = robotTwo.getTorsoColor();
                    robotOneColor.Offset = 0.0;
                    robotTwoColor.Offset = 0.85;
                    twoColors.GradientStops.Add(robotOneColor);
                    twoColors.GradientStops.Add(robotTwoColor);

                    lblWinnerColor.Background = twoColors;
                    lblWinnerRobot.Content = "draw! ";
                }

                    lblWinnerIs.Content = "The winner is:";
                    
                lblWinnerColor.Visibility = System.Windows.Visibility.Visible;
                lblWinnerColor.BorderBrush = new SolidColorBrush(Colors.Black);

                //race stopped, do what should be done. TN

                storyBoard.Pause(this); 
                raceIsOver();
            }
            //else: dont mind..
        }

        /** radioRandom_Checked():
         * We want a random opponent, do random
         * Uses int randomNumber(min, max) from WpfRobotBody class
         * TN
         * */
        private void radioRandom_Checked(object sender, RoutedEventArgs e)
        {
            rndOpponentType = WpfRobotBody.randomNumber(1, 3);
        }

        private void radioStadiumRace_Checked(object sender, RoutedEventArgs e)
        {
            // not really needed, just to be sure, TN
            rndOpponentType = 0;
        }



        private void radSideways_Click(object sender, RoutedEventArgs e)
        {
            cameraAngle = 1; // sideways
            handleCamera(sender, e);
        }

        private void radOverhead_Click(object sender, RoutedEventArgs e)
        {
            cameraAngle = 0; // overhead
            handleCamera(sender, e);
        }

        private void radXYPlane_Click(object sender, RoutedEventArgs e)
        {
            cameraAngle = 2; // xy-plane
            handleCamera(sender, e);
        }

        private void radPerspect_Click(object sender, RoutedEventArgs e)
        {
            cameraType = "perspective"; // perspective camera type
            handleCamera(sender, e);
        }

        private void radOrthograph_Click(object sender, RoutedEventArgs e)
        {
            cameraType = "orthographic"; // orthographic camera type
            handleCamera(sender, e);
        }

    }
}


