# The Walking robot with racing #
End project of the Computer Graphic, fall 2012.

## Technologies / topics  ##
* XAML user interface 
* Petzold.Media3D.dll 
* Storyboard 
* Animation
* Understanding and editing someone else's code 
* Understanding of 3D space and perspectives

## The program ##
This is the how the given program looked like before the task:
![utgspnkt.JPG](https://bitbucket.org/repo/orG87y/images/891698835-utgspnkt.JPG)

This is the result:
In the "play-box" you can change values with immediately response to the animation.
![resultat1.JPG](https://bitbucket.org/repo/orG87y/images/3259806775-resultat1.JPG)

You may also have a race between two figures. Note the opponent settings. Colors on the figures are selected randomly. 
![resultat2.JPG](https://bitbucket.org/repo/orG87y/images/4244452078-resultat2.JPG)

End of a race! Shown from the orthographic view. Note the race is a draw, and both red and blue is the winner (shown in the box). 
![resultat4.JPG](https://bitbucket.org/repo/orG87y/images/1698395999-resultat4.JPG)

## The assignment ##
(Copyrighted Sturla Pedersen, Buskerud and Vestfold University College) 
- Freely translated from Norwegian to English by Google translate: 

Part 1: Create a VS / C # / XAML / WPF project based on pointer 3. Main window will consist of two panels and a graphical window. A panel shall contain buttons, sliders and other controls required to make changes to the drawing objects. The second will contain of status information. 
The main window will be dynamic. That is, if we change the size, all window and figures shall be updated. Add a button "Quit" to exit  
the program. 
Part 2: Add color to the model so that the extremities inseparable. Add a button that starts / freeze the animation. 
Part 3: Note that the model's movements are composed of two sets: 1) limb movements on knee and hip joints, the arm movements on 
shoulder and elbow joints and 2) the body motion (animation) on the ground. Locate the variables that control these movements. 
Part 4: Transfer the angle changes from sliders to body movements (Item 1) 
Part 5: Add a control that regulates the body's motion (animation - Hint: You can adjust the clock associated with the animation. See, for example, Speedratio.xaml). 
Part 6: Add a control that switches between perspective and ortogonalprojeksjon. 
Part 7: Add a control that switches the camera position between the original lateral position, top in the XZ-plane. 

Other criteria: 
- Perspective and orthographic view for all three angles 
- Arms and legs can only bend in human directions

Base code: http://www.codeproject.com/Articles/125694/How-To-Make-A-Walking-Robot-In-WPF-Part-3-Emissive