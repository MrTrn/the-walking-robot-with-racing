/**     Obligatorisk oppgave GRAFDA60 
 *      2012: Terje Rene Nilsen 
 *      terje.nilsen@student.hive.no
 *      
 *      Mine kommentarer er p� engelsk.
 *      
 *      Koden til WpfRobotBody.randomNumber() er 
 *      opprinnelig fra Marc Gravell.
 *      Referanse gitt ved den aktuelle koden.
 *      
 *      Aksekorset er koden til Charles Petzold.
 *      Aksekorset inkl. f�lgende filer:
 *      Axes.cs, StrokeCharacters.cs, TextGenerator.cs
 *      Petzold.Media3D.dll
 *      
 *      Terje Rene Nilsen (TN)
 * */
//------------------------------------------------
// StrokeCharacter.cs (c) 2007 by Charles Petzold
//------------------------------------------------

//-------------------------------------------------------
// This class describes the elements of the 
// *FontResourceDictionary.xaml files.
//
// The class is internal to the Petzold.Medi3D assembly.
//-------------------------------------------------------
using System;
using System.Windows;
using System.Windows.Media;
using Petzold.Media3D;

namespace WpfRobot
{
    class StrokeCharacter
    {
        int width;
        Geometry geo;

        public StrokeCharacter(int width, string strGeometry)
        {
            Width = width;
            Geometry = Geometry.Parse(strGeometry);
        }

        // The Width of the font character.
        public int Width
        {
            set { width = value; }
            get { return width; }
        }

        // The Geometry that defines the font character.
        public Geometry Geometry
        {
            set { geo = value; }
            get { return geo; }
        }
    }
}